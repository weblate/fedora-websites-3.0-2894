const editions = [
  {
    name: "Fedora Workstation",
    href: "/workstation",
    icon: "desktop",
  },
  { name: "Fedora Server", href: "/server", icon: "server" },
  { name: "Fedora IoT", href: "/iot", icon: "microchip" },
  {
    name: "Fedora Cloud",
    href: "https://alt.fedoraproject.org/cloud/",
    icon: "cloud",
  },
  {
    name: "Fedora CoreOS",
    href: "/coreos",
    icon: "circle",
  },
];

const others = [
  { name: "Fedora Silverblue", href: "https://silverblue.fedoraproject.org/" },
  { name: "Fedora Kinoite", href: "https://kinoite.fedoraproject.org/" },
  { name: "Fedora Container Images", href: "https://hub.docker.com/_/fedora" },
  {
    name: "Fedora KDE",
    href: "https://spins.fedoraproject.org/en/kde/",
  },
  {
    name: "Fedora XFCE",
    href: "https://spins.fedoraproject.org/en/xfce/",
  },
  { name: "More...", href: "https://spins.fedoraproject.org" },
];

const community = [
  {
    name: "Flock To Fedora",
    href: "/flocktofedora",
  },
  { name: "Discussion Board", href: "https://discussion.fedoraproject.org/" },
  { name: "Community Blog", href: "https://communityblog.fedoraproject.org/" },
  { name: "Fedora Magazine", href: "https://fedoramagazine.org/" },
  { name: "Join Fedora", href: "#" },
  { name: "Matrix Chat Server", href: "#" },
  { name: "Fedora Account System", href: "#" },
  { name: "Fedora Developer Portal", href: "#" },
];

const support = [
  { name: "Fedora Help Forum", href: "https://ask.fedoraproject.org" },
  { name: "Documentation", href: "https://docs.fedoraproject.org/" },
  { name: "Common Bugs", href: "#" },
];

const about = "https://docs.fedoraproject.org/en-US/project/";

export { editions, others, community, support, about };
